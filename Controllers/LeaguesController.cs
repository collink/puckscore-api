﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PuckscoreApi;
using PuckscoreApi.Models;

namespace puckscore_api.Controllers
{
    [Produces("application/json")]
    [Route("api/Leagues")]
    public class LeaguesController : Controller
    {
        private PuckscoreContext _context { get; set; }

        public LeaguesController(PuckscoreContext context)
        {
            _context = context;
        }

        // GET: api/Leagues
        [HttpGet]
        public IEnumerable<League> Get()
        {
            return _context.Leagues;
        }

        // GET: api/Leagues/5
        [HttpGet("{id}", Name = "Get")]
        public League Get(int id)
        {
            return _context.Leagues.Where(l => l.ID == id).FirstOrDefault();
        }
        
        // POST: api/Leagues
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
        
        // PUT: api/Leagues/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
