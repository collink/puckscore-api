﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PuckscoreApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PuckscoreApi
{
    public class PuckscoreContext : DbContext
    {
        private string _Server { get; set; }
        private string _Database { get; set; }
        private string _User { get; set; }
        private string _Password { get; set; }
        private int _Timeout { get; set; }

        public DbSet<League> Leagues { get; set; }

        public PuckscoreContext(string server, string database, string user, string password, int timeout = 60)
        {
            _Server = server;
            _Database = database;
            _User = user;
            _Password = password;
            _Timeout = timeout;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string connectionString = $"server={_Server};database={_Database};user={_User};password={_Password};default command timeout={_Timeout}";
            optionsBuilder.UseMySQL(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<League>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.Name).IsRequired();
            });
        }
    }
}
