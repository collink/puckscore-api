﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PuckscoreApi.Models
{
    public class League
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
    }
}
