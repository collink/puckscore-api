﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PuckscoreApi.Models;

namespace PuckscoreApi
{
    public class Startup
    {
        private string _Server { get; set; }
        private string _Database { get; set; }
        private string _User { get; set; }
        private string _Password { get; set; }
        private int _Timeout { get; set; }

        public IConfiguration Configuration { get; }
        public PuckscoreContext Context { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            _Server = Configuration.GetValue<string>("DatabaseSettings:Server");
            _Database = Configuration.GetValue<string>("DatabaseSettings:Database");
            _User = Configuration.GetValue<string>("DatabaseSettings:User");
            _Password = Configuration.GetValue<string>("DatabaseSettings:Password");
            _Timeout = Configuration.GetValue<int>("DatabaseSettings:Timeout");

            Context = new PuckscoreContext(_Server, _Database, _User, _Password, _Timeout);
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddMvc();
            services.AddSingleton<PuckscoreContext>(Context);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseCors(builder =>
                   builder.WithOrigins("http://localhost:4200"));
            }

            app.UseMvc();
        }
    }
}
